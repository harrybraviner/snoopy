#include "wandering_forcing.h"

#include <math.h>
#include <complex.h>
#include "common.h"

#ifdef WANDERING_FORCING

/***************************************
 * Spatially random forcing
 * ************************************/

void wandering_forcing(struct Field fldi,
                    double dt) {
// Force random velocity field
	const double kf = 3.0 * M_PI * 2.0;
	const double deltakf = kf * 0.2;
    const double amplitude_forcing = 2.0;

    const double decay_timescale = 10.0;

	// Force all the vector
	
	
	int i,j,k;
	int num_force=0;
	int total_num_force;
	double fact;
	double q0;
	
	q0=pow(dt,0.5);
	
	for( i = 0; i < NX_COMPLEX/NPROC; i++) {
		for( j = 0; j < NY_COMPLEX; j++) {
			for( k = 0; k < NZ_COMPLEX; k++) {
				if( (k2t[ IDX3D ]>(kf-deltakf)*(kf-deltakf)) && (k2t[ IDX3D ]<(kf+deltakf)*(kf+deltakf))) {
					w4[ IDX3D ] = amplitude_forcing * mask[IDX3D] * randm() * cexp( I * 2.0*M_PI*randm() ) * q0;
					w5[ IDX3D ] = amplitude_forcing * mask[IDX3D] * randm() * cexp( I * 2.0*M_PI*randm() ) * q0;
					w6[ IDX3D ] = amplitude_forcing * mask[IDX3D] * randm() * cexp( I * 2.0*M_PI*randm() ) * q0;

					if(mask[IDX3D] > 0) num_force++;
				}
				else {
					w4[ IDX3D ] = 0.0;
					w5[ IDX3D ] = 0.0;
					w6[ IDX3D ] = 0.0;
				}
			}
		}
	}
	
  symmetrize_complex(w4);
  if(rank==0) w4[0]=0.0;
  symmetrize_complex(w5);
  if(rank==0) w5[0]=0.0;
  symmetrize_complex(w6);
  if(rank==0) w6[0]=0.0;
  
	// Get the total number of forced scales.
#ifdef MPI_SUPPORT
	MPI_Allreduce( &num_force, &total_num_force, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
#else
	total_num_force=num_force;
#endif
	
	fact=pow(total_num_force,0.5);

	for( i = 0; i < NX_COMPLEX/NPROC; i++) {
		for( j = 0; j < NY_COMPLEX; j++) {
			for( k = 0; k < NZ_COMPLEX; k++) {
				fldi.f1[ IDX3D ] += w4[ IDX3D ];
                fldi.f1[ IDX3D ] *= exp( - dt / decay_timescale );
				fldi.f2[ IDX3D ] += w5[ IDX3D ];
                fldi.f2[ IDX3D ] *= exp( - dt / decay_timescale );
				fldi.f3[ IDX3D ] += w6[ IDX3D ];
                fldi.f3[ IDX3D ] *= exp( - dt / decay_timescale );
			}
		}
	}

	for( i = 0; i < NX_COMPLEX/NPROC; i++) {
		for( j = 0; j < NY_COMPLEX; j++) {
			for( k = 0; k < NZ_COMPLEX; k++) {
				fldi.vx[ IDX3D ] += dt * ((double)NTOTAL)*fldi.f1[ IDX3D ] / fact;
				fldi.vy[ IDX3D ] += dt * ((double)NTOTAL)*fldi.f2[ IDX3D ] / fact;
				fldi.vz[ IDX3D ] += dt * ((double)NTOTAL)*fldi.f3[ IDX3D ] / fact;
			}
		}
	}
	
	projector(fldi.vx,fldi.vy,fldi.vz);
	
	return;
}

#endif
