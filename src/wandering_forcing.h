#ifndef __WANDERING_FORCING_H__
#define __WANDERING_FORCING_H__
#include "snoopy.h"

#ifdef WANDERING_FORCING
void wandering_forcing(struct Field fldi, double dt);
#endif
#endif
