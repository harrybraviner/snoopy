#! /usr/bin/python2.7

## For patching the error in the Fourier integrals if you have to
## restart a run. Need to locate the break manually and set params
## in this script accordingly

fin = open("timevar170.dat", "r")
fout = open("timevar170B.dat", "w")

restartMin = 1.74995e+3
restartMax = 1.75005e+3

header = fin.readline()

breakReached = False
breakintCRxy = 0
breakintSRxy = 0

for line in fin:
    line = line.split()
    line = map(float, line)
    if not breakReached:
        if line[0] > restartMin:
            breakReached = True
            breakintSRxy = line[10]
            breakintCRxy = line[11]
    else:
        line[10] += breakintSRxy
        line[11] += breakintCRxy
    for val in line:
        fout.write("{}\t".format(val))
    fout.write("\n")

fin.close()
fout.close()
